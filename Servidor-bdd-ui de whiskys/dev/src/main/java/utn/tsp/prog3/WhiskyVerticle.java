package utn.tsp.prog3;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;

public class WhiskyVerticle extends AbstractVerticle {

	private PgPool client;

	@Override
	public void start(Promise<Void> promise) throws Exception {

		// BASE DE DATOS
		PgConnectOptions connectOptions = new PgConnectOptions().setPort(5432).setHost("127.0.0.1")
				.setDatabase("postgres").setUser("postgres").setPassword("edu");
		// Pool options
		PoolOptions poolOptions = new PoolOptions().setMaxSize(5);
		// Create the client pool
		this.client = PgPool.pool(vertx, connectOptions, poolOptions);
		WhiskyManager whiskyManager = new WhiskyManager(this.client);

		// RUTAS
		Router router = Router.router(vertx);
		router.get("/").handler(rc -> rc.response().end("Hello World!"));
		router.get("/:name").handler(rc -> rc.response().end("Hello " + rc.pathParam("name")));

		router.route("/api/whiskies*").handler(BodyHandler.create());

		router.get("/api/whiskies").handler(whiskyManager::getAll);
		router.post("/api/whiskies").handler(whiskyManager::addOne);
		router.delete("/api/whiskies/:id").handler(whiskyManager::deleteOne);
		router.get("/api/whiskies/:id").handler(whiskyManager::getOne);
		router.put("/api/whiskies/:id").handler(whiskyManager::updateOne);

		// Serve static resources from the /assets directory
		router.route("/assets/*").handler(StaticHandler.create("assets"));

		//Http server
		vertx.createHttpServer().requestHandler(router).listen(8080, result -> {
			if (result.succeeded()) {
				System.out.println("Server is now listening :)!");
				promise.complete();
			} else {
				System.out.println("Failed to bind!");
				promise.fail(result.cause());
			}
		});

	}

}
