module WhiskyFx {
	requires javafx.controls;
	requires javafx.fxml;
	//requires javafx.graphics;
	//requires javafx.base;
	//requires vertx.web.client;
	//requires vertx.core;
	requires io.vertx.web.client;
	requires java.net.http;
	requires io.vertx.core;

	opens utn.frp.p3.application.view to javafx.graphics, javafx.fxml;
	opens utn.frp.p3 to javafx.graphics, javafx.fxml;
	opens utn.frp.p3.application.middleware to io.vertx.web.client;
}
