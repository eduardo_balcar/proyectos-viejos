#-------------------------------------------------
#
# Project created by QtCreator 2015-11-19T12:30:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = examen1
TEMPLATE = app


SOURCES += main.cpp\
        central.cpp \
    registro.cpp \
    warning.cpp

HEADERS  += central.h \
    registro.h \
    warning.h

FORMS    += central.ui \
    warning.ui
