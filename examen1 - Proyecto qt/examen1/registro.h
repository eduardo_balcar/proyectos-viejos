#ifndef REGISTRO_H
#define REGISTRO_H

#include<fstream>
#include<string>

using namespace::std;

typedef struct DATOS{
    string anio;
    string mes;
    string dia;
    string hora;
    string minuto;
    string segundo;
    string interno;
    string clave;
    string destino;
}datos;

class registro
{
private:
    ifstream archivo;
    datos *vectordatos;
    string linea;
    int cantidad;
    string nombre;
    string tel;

public:
    registro();
    bool procesar(string, string telefono);
    int getcantidad();
    string getinterno();
    string getiempo();

};

#endif // REGISTRO_H
