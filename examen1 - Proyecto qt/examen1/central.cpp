#include "central.h"
#include "ui_central.h"
#include <warning.h>
#include <ui_warning.h>

central::central(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::central)
{
    ui->setupUi(this);
}

central::~central()
{
    delete ui;
}

void central::on_cargar_clicked()
{



    if(ui->destino->text().compare("")==0)e.show();
    else{
    QString nombreArchivo = QFileDialog::getOpenFileName(this,"Archivo de datos de registro",
                                                         QApplication::applicationDirPath(),
                                                         "Archivos de datos (*.txt);;Todos los archivos (*.*)");
        if(nombreArchivo != NULL)
        {
           if( reg.procesar(nombreArchivo.toStdString(),ui->destino->text().toStdString()) ){
               ui->cantidad->setText(QString::number(reg.getcantidad()));
               ui->duracion->setText(QString::fromStdString(reg.getiempo()));
               ui->interno->setText(QString::fromStdString(reg.getinterno()));
           }
           else ui->interno->setText("IMPOSIBLE PROCESAR");

        }
        }



}
