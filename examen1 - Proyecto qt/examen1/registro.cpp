#include "registro.h"
#include <cstring>
#include <string.h>

registro::registro()
{

}

bool registro::procesar(string nom, string telefono){
    bool ok=true;
    int i=0;
    tel=telefono;
    nombre=nom;
    cantidad=0;

    archivo.open(nom.c_str());
    if(archivo){
        while (getline(archivo,linea)) {
           cantidad++;
        }

        vectordatos = new datos [cantidad];
        archivo.clear();
        archivo.seekg(0,ios::beg);
        while(getline(archivo,linea)){
            /*
            vectordatos[i].anio = linea.substr(0,4);
            vectordatos[i].mes = linea.substr(5,2);
            vectordatos[i].dia = linea.substr(8,2);
            vectordatos[i].hora = linea.substr(11,2);
            vectordatos[i].minuto = linea.substr(14,2);
            vectordatos[i].segundo = linea.substr(17,2);
            vectordatos[i].interno = linea.substr(20,3);
            vectordatos[i].clave = linea.substr(24,1);
            vectordatos[i].destino = linea.substr(26,linea.length()-26);
            i++;*/
            vectordatos[i].anio = linea.substr(0,4);
            vectordatos[i].mes = linea.substr(4,2);
            vectordatos[i].dia = linea.substr(6,2);
            vectordatos[i].hora = linea.substr(8,2);
            vectordatos[i].minuto = linea.substr(10,2);
            vectordatos[i].segundo = linea.substr(12,2);
            vectordatos[i].interno = linea.substr(14,3);
            vectordatos[i].clave = linea.substr(17,1);
            vectordatos[i].destino = linea.substr(18,linea.length()-18);
            i++;
        }


    }
    else ok=false;

    archivo.close();
    return ok;
}

string registro::getiempo(){
    int i;
    string aux="EEROR";

    for (i=0;i<cantidad;i++){
        //if(vectordatos[i].destino == tel){

        if(vectordatos[i].destino.compare(tel)==0){
            aux="El total del tiempo es: ";
            aux+= vectordatos[i].hora;
            aux+= " hs, ";
            aux+= vectordatos[i].minuto;
            aux+= " min y ";
            aux+= vectordatos[i].segundo;
            aux+= " seg.";
        }
    }
    return aux;
}

string registro::getinterno(){
    string horalarga;
    string minlargo;
    string seglargo;
    string aux;
    int i;


    horalarga=vectordatos[0].hora;
    minlargo=vectordatos[0].minuto;
    seglargo=vectordatos[0].segundo;
    aux=vectordatos[0].interno;

    for(i=1;i<cantidad;i++){
        if(horalarga<vectordatos[i].hora || horalarga==vectordatos[i].hora){
            if(minlargo<vectordatos[i].minuto || minlargo==vectordatos[i].minuto){
                if(seglargo<vectordatos[i].segundo)aux=vectordatos[i].interno;
            }
        }
    }

    return aux;
}

int registro::getcantidad(){
    return cantidad;
}
