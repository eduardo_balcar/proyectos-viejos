#ifndef CENTRAL_H
#define CENTRAL_H

#include <QDialog>
#include<registro.h>
#include<QFileDialog>
#include <warning.h>

namespace Ui {
class central;
}

class central : public QDialog
{
    Q_OBJECT

public:
    explicit central(QWidget *parent = 0);
    ~central();
    registro reg;

private slots:
    void on_cargar_clicked();

private:
    Ui::central *ui;
    Warning e;
};

#endif // CENTRAL_H
