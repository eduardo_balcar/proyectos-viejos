/********************************************************************************
** Form generated from reading UI file 'central.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CENTRAL_H
#define UI_CENTRAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_central
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QLineEdit *destino;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *cargar;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *cantidad;
    QLineEdit *interno;
    QLineEdit *duracion;

    void setupUi(QDialog *central)
    {
        if (central->objectName().isEmpty())
            central->setObjectName(QStringLiteral("central"));
        central->resize(400, 220);
        verticalLayout_2 = new QVBoxLayout(central);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(central);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        destino = new QLineEdit(central);
        destino->setObjectName(QStringLiteral("destino"));

        horizontalLayout->addWidget(destino);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        cargar = new QPushButton(central);
        cargar->setObjectName(QStringLiteral("cargar"));

        horizontalLayout_2->addWidget(cargar);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_2 = new QLabel(central);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        label_3 = new QLabel(central);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        label_4 = new QLabel(central);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_3->addWidget(label_4);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        cantidad = new QLineEdit(central);
        cantidad->setObjectName(QStringLiteral("cantidad"));

        horizontalLayout_4->addWidget(cantidad);

        interno = new QLineEdit(central);
        interno->setObjectName(QStringLiteral("interno"));

        horizontalLayout_4->addWidget(interno);

        duracion = new QLineEdit(central);
        duracion->setObjectName(QStringLiteral("duracion"));

        horizontalLayout_4->addWidget(duracion);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(central);

        QMetaObject::connectSlotsByName(central);
    } // setupUi

    void retranslateUi(QDialog *central)
    {
        central->setWindowTitle(QApplication::translate("central", "Central Telefonica", 0));
        label->setText(QApplication::translate("central", "Nro Destino:", 0));
        cargar->setText(QApplication::translate("central", "Cargar Registro", 0));
        label_2->setText(QApplication::translate("central", "Llamadas", 0));
        label_3->setText(QApplication::translate("central", "Interno ", 0));
        label_4->setText(QApplication::translate("central", "Duracion en segundos", 0));
    } // retranslateUi

};

namespace Ui {
    class central: public Ui_central {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CENTRAL_H
