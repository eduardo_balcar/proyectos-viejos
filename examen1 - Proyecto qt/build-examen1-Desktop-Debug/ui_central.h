/********************************************************************************
** Form generated from reading UI file 'central.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CENTRAL_H
#define UI_CENTRAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_central
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QLineEdit *destino;
    QPushButton *cargar;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *cantidad;
    QLineEdit *interno;
    QLineEdit *duracion;

    void setupUi(QDialog *central)
    {
        if (central->objectName().isEmpty())
            central->setObjectName(QString::fromUtf8("central"));
        central->resize(587, 220);
        verticalLayout_2 = new QVBoxLayout(central);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(central);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        destino = new QLineEdit(central);
        destino->setObjectName(QString::fromUtf8("destino"));

        horizontalLayout->addWidget(destino);

        cargar = new QPushButton(central);
        cargar->setObjectName(QString::fromUtf8("cargar"));

        horizontalLayout->addWidget(cargar);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_2 = new QLabel(central);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_3->addWidget(label_2);

        label_3 = new QLabel(central);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        label_4 = new QLabel(central);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_3->addWidget(label_4);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        cantidad = new QLineEdit(central);
        cantidad->setObjectName(QString::fromUtf8("cantidad"));
        cantidad->setReadOnly(true);

        horizontalLayout_4->addWidget(cantidad);

        interno = new QLineEdit(central);
        interno->setObjectName(QString::fromUtf8("interno"));
        interno->setEnabled(true);
        interno->setDragEnabled(false);
        interno->setReadOnly(true);
        interno->setCursorMoveStyle(Qt::VisualMoveStyle);

        horizontalLayout_4->addWidget(interno);

        duracion = new QLineEdit(central);
        duracion->setObjectName(QString::fromUtf8("duracion"));
        duracion->setReadOnly(true);

        horizontalLayout_4->addWidget(duracion);


        verticalLayout->addLayout(horizontalLayout_4);


        horizontalLayout_2->addLayout(verticalLayout);


        verticalLayout_2->addLayout(horizontalLayout_2);


        retranslateUi(central);

        QMetaObject::connectSlotsByName(central);
    } // setupUi

    void retranslateUi(QDialog *central)
    {
        central->setWindowTitle(QCoreApplication::translate("central", "Central Telefonica", nullptr));
        label->setText(QCoreApplication::translate("central", "Nro Destino:", nullptr));
        cargar->setText(QCoreApplication::translate("central", "Cargar Registro", nullptr));
        label_2->setText(QCoreApplication::translate("central", "Total llamadas", nullptr));
        label_3->setText(QCoreApplication::translate("central", "Interno mayor llamada", nullptr));
        label_4->setText(QCoreApplication::translate("central", "Duracion llamada destino", nullptr));
        cantidad->setPlaceholderText(QCoreApplication::translate("central", "Cantidad", nullptr));
        interno->setPlaceholderText(QCoreApplication::translate("central", "Nro Interno", nullptr));
        duracion->setPlaceholderText(QCoreApplication::translate("central", "Duracion", nullptr));
    } // retranslateUi

};

namespace Ui {
    class central: public Ui_central {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CENTRAL_H
