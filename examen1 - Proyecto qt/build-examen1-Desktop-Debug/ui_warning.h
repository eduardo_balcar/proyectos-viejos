/********************************************************************************
** Form generated from reading UI file 'warning.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WARNING_H
#define UI_WARNING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QPlainTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Warning
{
public:
    QDialogButtonBox *buttonBox;
    QFrame *frame;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QDialog *Warning)
    {
        if (Warning->objectName().isEmpty())
            Warning->setObjectName(QString::fromUtf8("Warning"));
        Warning->resize(400, 300);
        buttonBox = new QDialogButtonBox(Warning);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        frame = new QFrame(Warning);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(20, 30, 351, 171));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        plainTextEdit = new QPlainTextEdit(frame);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(30, 30, 291, 111));

        retranslateUi(Warning);
        QObject::connect(buttonBox, SIGNAL(accepted()), Warning, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Warning, SLOT(reject()));

        QMetaObject::connectSlotsByName(Warning);
    } // setupUi

    void retranslateUi(QDialog *Warning)
    {
        Warning->setWindowTitle(QCoreApplication::translate("Warning", "Dialog", nullptr));
        plainTextEdit->setPlainText(QCoreApplication::translate("Warning", "No ingreso un numero de telfono antes de cargar el archivo.\n"
"Por favor ingrese un numero de telefono para poder procesar el archivo.", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Warning: public Ui_Warning {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WARNING_H
