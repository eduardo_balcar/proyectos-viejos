package builderEjercicio3;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.time.LocalDate;
//import java.awt.List;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio3 extends JFrame {

	private JPanel contentPane;
	protected JPanel panel;
	protected JLabel lblNewLabel;
	protected JLabel lblContrasea;
	protected JTextField txtUser;
	protected JTextField txtPass;
	protected JLabel lbFecha;
	protected JButton btnAlta;
	protected JButton btnBaja;
	protected JButton btnModificacion;
	protected JButton btnPrimero;
	protected JButton btnUltimo;
	protected JButton btnSiguiente;
	protected JButton btnSalir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		String[] usuarios = { "Primero", "Segundo", "Tercero", "Cuarto", "Quinto", "Sexto", "Septimo", "Octavo",
				"Noveno", "Decimo" };
		String[] contraseña = { "uno", "dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez" };

		for (int i = 0; i < 10; i++) {
			Usuario unUsuario = new Usuario(usuarios[i], contraseña[i], new Date());
			listaUsuarios.add(unUsuario);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					Ejercicio3 frame = new Ejercicio3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static List<Usuario> listaUsuarios = new ArrayList<Usuario>();
	static SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	Iterator<Usuario> iterador = listaUsuarios.iterator();

	/**
	 * Create the frame.
	 */

	public Ejercicio3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		this.panel = new JPanel();
		this.contentPane.add(this.panel, BorderLayout.CENTER);
		this.panel.setLayout(null);

		this.lblNewLabel = new JLabel("Usuario");
		this.lblNewLabel.setBounds(31, 23, 70, 15);
		this.panel.add(this.lblNewLabel);

		this.lblContrasea = new JLabel("Contraseña");
		this.lblContrasea.setBounds(31, 54, 70, 15);
		this.panel.add(this.lblContrasea);

		this.txtUser = new JTextField();
		this.txtUser.setBounds(137, 21, 114, 19);
		this.panel.add(this.txtUser);
		this.txtUser.setColumns(10);

		this.txtPass = new JTextField();
		this.txtPass.setBounds(137, 52, 114, 19);
		this.panel.add(this.txtPass);
		this.txtPass.setColumns(10);

		this.lbFecha = new JLabel("");
		this.lbFecha.setBounds(31, 227, 348, 15);
		this.panel.add(this.lbFecha);

		this.btnAlta = new JButton("Alta");
		this.btnAlta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (txtUser.getText().equals("") || txtPass.getText().equals(""))
					JOptionPane.showMessageDialog(null,new JTextArea("Debe\nIngresar\tun\n\tnombre"));
				else {
					try {
					Usuario nuevo = new Usuario(txtUser.getText(), txtPass.getText(), new Date());
					if (listaUsuarios.contains(nuevo))
						throw new ExcepcionUsuario();
					else {
						listaUsuarios.add(nuevo);
						JOptionPane.showMessageDialog(null, "Agregado correctamente");
					}
					}
					catch(ExcepcionUsuario b) {
						JOptionPane.showMessageDialog(null, b.getMessage());
					}
				}
			}
		});
		this.btnAlta.setBounds(12, 119, 117, 25);
		this.panel.add(this.btnAlta);

		this.btnBaja = new JButton("Baja");
		this.btnBaja.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Usuario nuevo=new Usuario (txtUser.getText(),txtPass.getText(),null);
				if(listaUsuarios.remove(nuevo))JOptionPane.showMessageDialog(null, "Borrado");
				else JOptionPane.showMessageDialog(null, "No se encontro el elemento");
			}
		});
		this.btnBaja.setBounds(137, 119, 117, 25);
		this.panel.add(this.btnBaja);

		this.btnModificacion = new JButton("Modificacion");
		this.btnModificacion.setBounds(262, 119, 117, 25);
		this.panel.add(this.btnModificacion);

		this.btnPrimero = new JButton("Primero");
		this.btnPrimero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Usuario aux = (Usuario) listaUsuarios.get(0);
				iterador = listaUsuarios.iterator();
				txtUser.setText(aux.getUsuario());
				txtPass.setText(aux.getContraseña());
				lbFecha.setText("Ultima actualizacion " + formatoFecha.format(aux.getFechaActualizacion()));
			}
		});
		this.btnPrimero.setBounds(12, 156, 117, 25);
		this.panel.add(this.btnPrimero);

		this.btnUltimo = new JButton("Ultimo");
		this.btnUltimo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Usuario aux = (Usuario) listaUsuarios.get(listaUsuarios.size() - 1);
				txtUser.setText(aux.getUsuario());
				txtPass.setText(aux.getContraseña());
				lbFecha.setText("Ultima actualizacion " + formatoFecha.format(aux.getFechaActualizacion()));
			}
		});
		this.btnUltimo.setBounds(137, 156, 117, 25);
		this.panel.add(this.btnUltimo);

		this.btnSiguiente = new JButton("Siguiente");
		this.btnSiguiente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if (iterador.hasNext()) {
						Usuario aux = (Usuario) iterador.next();
						txtUser.setText(aux.getUsuario());
						txtPass.setText(aux.getContraseña());
						lbFecha.setText("Ultima actualizacion " + formatoFecha.format(aux.getFechaActualizacion()));
					} else
						throw new ExcepcionUsuario();
				} catch (ExcepcionUsuario b) {
					JOptionPane.showMessageDialog(null, b.getMessage());
				}
			}
		});
		this.btnSiguiente.setBounds(262, 156, 117, 25);
		this.panel.add(this.btnSiguiente);

		this.btnSalir = new JButton("Fin");
		this.btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		this.btnSalir.setBounds(137, 193, 117, 25);
		this.panel.add(this.btnSalir);
	}
}
