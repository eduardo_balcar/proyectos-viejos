package builderEjercicio3;

public class ExcepcionUsuario extends Exception{

	public ExcepcionUsuario() {
		super("error en Usuario");
		// TODO Auto-generated constructor stub
	}

	public ExcepcionUsuario(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ExcepcionUsuario(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ExcepcionUsuario(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExcepcionUsuario(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
