package builderEjercicio3;

import java.util.Date;

public class Usuario implements Comparable {
	private String usuario;
	private String contraseña;
	private Date fechaActualizacion;
	
	public Usuario(String usuario, String contraseña, Date fechaActualizacion) {
		super();
		this.usuario = usuario;
		this.contraseña = contraseña;
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@Override
	public int compareTo(Object arg0) {
		
		return this.getUsuario().compareTo(((Usuario) arg0).getUsuario());
	}

	@Override
	public boolean equals(Object obj) {
		Usuario arg0=(Usuario) obj;
		return arg0.getUsuario().equals(this.getUsuario());
	}
	

}
