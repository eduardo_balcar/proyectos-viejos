package ventana;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import clases.ExcepcionDatos;
import clases.Persistencia;
import clases.Socio;

public class ClubDeBarrio extends JFrame {
	private JTable tabla;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;
	private JTextField txtNroSocio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClubDeBarrio frame = new ClubDeBarrio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static Map<Long, Socio> lista = new TreeMap<Long, Socio>();
	static List<Socio> arraySocios = new ArrayList<Socio>();
	DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private JFormattedTextField txt;

	/**
	 * Create the frame.
	 * 
	 * @throws ParseException
	 */
	public ClubDeBarrio() throws ParseException {
		setFont(new Font("DejaVu Sans", Font.BOLD, 13));
		setTitle("Club de Barrio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 519, 340);
		getContentPane().setLayout(null);
		
		JLabel labins = new JLabel("");
		labins.setBounds(205, 0, 70, 15);
		getContentPane().add(labins);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 141, 487, 151);
		getContentPane().add(scrollPane);

		JComboBox<String> cboxSocio = new JComboBox<String>();
		cboxSocio.setMaximumRowCount(2);
		cboxSocio.setModel(new DefaultComboBoxModel<String>(new String[] { "Socio", "Exsocio" }));
		cboxSocio.setSelectedIndex(1);
		cboxSocio.setBounds(195, 46, 80, 18);
		getContentPane().add(cboxSocio);

		txt = new JFormattedTextField(new MaskFormatter("##/##/####"));
		txt.setFont(new Font("Source Code Pro Medium", Font.PLAIN, 13));
		txt.setBounds(190, 76, 85, 18);
		getContentPane().add(txt);

		tabla = new JTable();
		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtNroSocio.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0)));
				txtNombre.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 1)));
				txtApellido.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 2)));
				txtDni.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 3)));
				txt.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 4)).formatted(formatoFecha));
				if (String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 5)).equals("Socio"))
					cboxSocio.setSelectedIndex(0);
				else
					cboxSocio.setSelectedIndex(1);

			}
		});
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabla.setFont(new Font("Droid Sans Mono", Font.PLAIN, 11));
		tabla.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Nro de Socio", "Nombre", "Apellido", "Dni", "Fecha Modificacion", "Estado"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(tabla);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(12, 104, 100, 25);
		getContentPane().add(btnSalir);

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/*Object[] key = lista.keySet().toArray();
				for (int i = 0; i < key.length; i++) {
					Socio socio = lista.get(key[i]);
					arraySocios.add(socio);
					
				}*/
				//arraySocios=(List<Socio>) lista.values();
				arraySocios=new ArrayList<Socio>(lista.values());
				Persistencia.escribir(arraySocios);

			}
		});
		btnGuardar.setBounds(12, 59, 100, 25);
		getContentPane().add(btnGuardar);

		JButton btnLeer = new JButton("Leer");
		btnLeer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lista.clear();
				List<String> datos = Persistencia.leer2();
				try {
				if(datos==null)throw new ExcepcionDatos("Archivo inexistente");
				for (String s : datos) {
					String[] sub = s.split(";");
					Socio socio = new Socio(Long.parseLong(sub[0]), LocalDate.parse(sub[1]), sub[2], sub[3],
							Long.parseLong(sub[4]));
					if (sub[5].equals("false"))
						socio.setSocioactual(false);
					lista.put(socio.getNrosocio(), socio);
				}
				labins.setText(String.valueOf(Socio.getContadorInstancias()));
				}catch(ExcepcionDatos E) {
					JOptionPane.showMessageDialog(null, E.getMessage());
				}

				mostrar();
			}

		});
		btnLeer.setBounds(12, 12, 100, 25);
		getContentPane().add(btnLeer);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

				try {
					if (lista.containsKey(Long.parseLong(txtNroSocio.getText()))) {
						Socio socio = lista.get(Long.parseLong(txtNroSocio.getText()));
						Object[] datos = socio.toString().split(";");
						modelo.setRowCount(0);
						modelo.addRow(datos);
					} else
						throw new Exception("Socio no encontrado");
				} catch (Exception E) {
					JOptionPane.showMessageDialog(null, E.getMessage());
				}
			}
		});
		btnBuscar.setBounds(175, 106, 100, 25);
		getContentPane().add(btnBuscar);

		JButton btnQuitar = new JButton("Quitar");
		btnQuitar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if (lista.containsKey(Long.parseLong(txtNroSocio.getText()))) {
						Socio socio = lista.get(Long.parseLong(txtNroSocio.getText()));
						if (socio.getSocioactual()) {
							socio.setSocioactual(false);
							socio.setFechainscripcion(LocalDate.now());
							lista.replace(Long.parseLong(txtNroSocio.getText()), socio);
							mostrar();
						} else
							throw new ExcepcionDatos("El socio ya esta dado de baja");
					} else
						throw new ExcepcionDatos("Socio no encontrado");
				}

				catch (ExcepcionDatos E) {
					JOptionPane.showMessageDialog(null, E.getMessage());
				}
			}
		});
		btnQuitar.setBounds(399, 106, 100, 25);
		getContentPane().add(btnQuitar);

		JButton btnAgregar = new JButton("Agregar");

		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if (lista.containsKey(Long.parseLong(txtNroSocio.getText()))) {
						Socio socio = lista.get(Long.parseLong(txtNroSocio.getText()));
						if (socio.getSocioactual())
							throw new ExcepcionDatos("Ya agregado");
						else {
							socio.setSocioactual(true);
							socio.setFechainscripcion(LocalDate.now());
							lista.replace(socio.getNrosocio(), socio);
						}
					} else {
						// throw new ExcepcionDatos("Ya agregado");
						long nro = Long.parseLong(txtNroSocio.getText());
						LocalDate fecha = LocalDate.parse(txt.getValue().toString(), formatoFecha);
						String nombre = txtNombre.getText();
						String apellido = txtApellido.getText();
						long dni = Long.parseLong(txtDni.getText());
						Socio socio = new Socio(nro, fecha, nombre, apellido, dni);
						lista.put(socio.getNrosocio(), socio);
					}
					mostrar();
					labins.setText(String.valueOf(Socio.getContadorInstancias()));

				} catch (Exception E) {
					JOptionPane.showMessageDialog(null, E.getMessage());
				}
			}
		});
		btnAgregar.setBounds(287, 106, 100, 25);
		getContentPane().add(btnAgregar);

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Source Code Pro Medium", Font.PLAIN, 13));
		txtNombre.setBounds(349, 12, 150, 18);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setFont(new Font("Source Code Pro Medium", Font.PLAIN, 13));
		txtApellido.setColumns(10);
		txtApellido.setBounds(349, 42, 150, 18);
		getContentPane().add(txtApellido);

		txtDni = new JTextField();
		txtDni.setFont(new Font("Source Code Pro Medium", Font.PLAIN, 13));
		txtDni.setColumns(10);
		txtDni.setBounds(349, 74, 150, 18);
		getContentPane().add(txtDni);

		txtNroSocio = new JTextField();
		txtNroSocio.setFont(new Font("Source Code Pro Medium", Font.PLAIN, 13));
		txtNroSocio.setColumns(10);
		txtNroSocio.setBounds(205, 16, 70, 18);
		getContentPane().add(txtNroSocio);

		
		
		
		JLabel lblNewLabel = new JLabel("Nro Socio");
		lblNewLabel.setBounds(132, 17, 70, 15);
		getContentPane().add(lblNewLabel);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(287, 17, 70, 15);
		getContentPane().add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(287, 46, 70, 15);
		getContentPane().add(lblApellido);

		JLabel lblNombre_1_1 = new JLabel("DNI");
		lblNombre_1_1.setBounds(287, 77, 70, 15);
		getContentPane().add(lblNombre_1_1);

		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(132, 46, 70, 15);
		getContentPane().add(lblEstado);

		JLabel lblFechaMod = new JLabel("Fecha  ");
		lblFechaMod.setBounds(132, 79, 50, 15);
		getContentPane().add(lblFechaMod);
		
		

	}

	protected void mostrar() {

		Object[] key = lista.keySet().toArray();
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		modelo.setNumRows(0);

		for (int i = 0; i < key.length; i++) {
			String estado = "ExSocio";
			Socio aux = lista.get(key[i]);
			if (aux.getSocioactual())
				estado = "Socio";

			Object[] renglon = { aux.getNrosocio(), aux.getNombre(), aux.getApellido(), aux.getDni(),
					aux.getFechainscripcion().format(formatoFecha), estado };
			// System.out.println(aux.toString());
			modelo.addRow(renglon);
		}

	}
}

//String[] dato=Persistencia.leer();
//String[] subdatos;
//for(int i=0;i<dato.length;i++) {
// subdatos=dato[0].split(";");
// Socio socio=new
// Socio(Long.parseLong(subdatos[0]),LocalDate.parse(subdatos[1]),subdatos[2],subdatos[3],Long.parseLong(subdatos[4]));
// if(subdatos[5].equals("false"))socio.setSocioactual(false);
// lista.put(socio.getNrosocio(), socio);
