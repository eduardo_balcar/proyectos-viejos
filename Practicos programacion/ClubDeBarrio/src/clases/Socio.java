package clases;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

public class Socio extends Persona {

	

	static AtomicInteger contadorInstancias = new AtomicInteger(0);
	
	public static int getContadorInstancias() {
		return contadorInstancias.get();
	}

	

	private long nrosocio;
	private LocalDate fechainscripcion;
	private boolean socioactual;
	private long prueba = 2;

	public Socio(String nombre, String apellido, long dni) {
		super(nombre, apellido, dni);
		contadorInstancias.incrementAndGet();
		// TODO Auto-generated constructor stub
	}

	public Socio(long nrosocio, LocalDate fechainscripcion, String nombre, String apellido, long dni) {
		super(nombre, apellido, dni);
		this.nrosocio = nrosocio;
		this.fechainscripcion = fechainscripcion;
		this.socioactual = true;
		contadorInstancias.incrementAndGet();

	}

	public Socio() {
		super();
	}

	public long getNrosocio() {
		return nrosocio;
	}

	public void setNrosocio(long nrosocio) {
		this.nrosocio = nrosocio;
	}

	public LocalDate getFechainscripcion() {
		return fechainscripcion;
	}

	public void setFechainscripcion(LocalDate fechainscripcion) {
		this.fechainscripcion = fechainscripcion;
	}

	public boolean getSocioactual() {
		return socioactual;
	}

	public void setSocioactual(boolean socioactual) {
		this.socioactual = socioactual;
	}
	
	@Override
	public String toString() {
		return String.valueOf(nrosocio) + ";" + String.valueOf(fechainscripcion) + ";" + this.getNombre() + ";"
				+ this.getApellido() + ";" + String.valueOf(this.getDni()) + ";" + String.valueOf(socioactual);
	}

}
