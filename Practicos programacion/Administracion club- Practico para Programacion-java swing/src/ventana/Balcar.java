package ventana;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
//import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import clases.ExcepcionDatos;
import clases.Persistencia;
import clases.Socio;

public class Balcar extends JFrame {

	private JPanel contentPane;
	private JTable tabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Balcar frame = new Balcar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Map<Long, Socio> listaPpal = new TreeMap<Long, Socio>();
	Set<Socio> listaXnom = new TreeSet<Socio>();
	private JTextField txtNro;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;

	/**
	 * Create the frame.
	 */
	public Balcar() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 549, 367);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JCheckBox checkNombre = new JCheckBox("Listar por Nombre");
		checkNombre.setBounds(137, 1, 160, 23);
		contentPane.add(checkNombre);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 187, 517, 132);
		contentPane.add(scrollPane);

		tabla = new JTable();
		tabla.setModel(new DefaultTableModel(new Object[][] { {}, {}, {}, }, new String[] {}));
		scrollPane.setViewportView(tabla);

		JButton btnleer = new JButton("Cargar Soc.");
		btnleer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cargarArchivo();
				
				if(checkNombre.isSelected())mostrar2();
				else mostrar(listaPpal);
			}
		});
		btnleer.setBounds(12, 0, 117, 25);
		contentPane.add(btnleer);

		JButton btnguardar = new JButton("Guardar");
		btnguardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				guardar(listaPpal);
			}
		});
		btnguardar.setBounds(12, 37, 117, 25);
		contentPane.add(btnguardar);

		JButton btnbuscar = new JButton("Buscar");
		btnbuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscar();
			}
		});
		btnbuscar.setBounds(12, 75, 117, 25);
		contentPane.add(btnbuscar);

		JButton btndeudor = new JButton("Deudores");
		btndeudor.setBounds(12, 112, 117, 25);
		contentPane.add(btndeudor);

		JButton btnagregar = new JButton("Agregar");
		btnagregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				agregar();
			}
		});
		btnagregar.setBounds(12, 149, 117, 25);
		contentPane.add(btnagregar);

		JButton btnquitar = new JButton("Quitar");
		btnquitar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				quitar();
			}
		});
		btnquitar.setBounds(141, 150, 117, 25);
		contentPane.add(btnquitar);

		JButton btnsalir = new JButton("Salir");
		btnsalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		btnsalir.setBounds(270, 150, 117, 25);
		contentPane.add(btnsalir);

		txtNro = new JTextField();
		txtNro.setBounds(214, 26, 100, 18);
		contentPane.add(txtNro);
		txtNro.setColumns(10);

		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(214, 59, 100, 18);
		contentPane.add(txtNombre);

		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(214, 89, 100, 18);
		contentPane.add(txtApellido);

		txtDni = new JTextField();
		txtDni.setColumns(10);
		txtDni.setBounds(214, 119, 100, 18);
		contentPane.add(txtDni);

		JLabel lblNewLabel = new JLabel("Nro Soc.");
		lblNewLabel.setBounds(141, 27, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(141, 64, 70, 15);
		contentPane.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(141, 90, 70, 15);
		contentPane.add(lblApellido);

		JLabel lblDni = new JLabel("Dni");
		lblDni.setBounds(141, 120, 70, 15);
		contentPane.add(lblDni);

		
	}

	/*-------------------------------METODOS------------------------------------------*/

	protected void mostrar2() {
		Map<String,Socio>lista2 = new TreeMap<String, Socio>();
		Object[] key = listaPpal.keySet().toArray();
		for(int i=0;i<key.length;i++) {
			Socio s=(Socio)listaPpal.get(key[i]);
			lista2.put(s.getNombre()+s.getApellido(), s);
		}
		mostrar3(lista2);
	}



	

	private void mostrar3(Map<String, Socio> lista2) {
		Object[] key = lista2.keySet().toArray();
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		modelo.setNumRows(0);
		modelo.setColumnCount(0);
		modelo.addColumn("Nro Socio");
		modelo.addColumn("Nombre");
		modelo.addColumn("Apellido");
		modelo.addColumn("Dni");
		modelo.addColumn("Fecha Insc");
		modelo.addColumn("Ult. Pago");

		for (int i = 0; i < key.length; i++) {
			Socio aux = lista2.get(key[i]);
			Object[] renglon = aux.toString().split(";");
			// System.out.println(aux.toString());
			modelo.addRow(renglon);
		}

		
	}

	protected void quitar() {
		try {
			txtNro.setText(String.valueOf(tabla.getValueAt(tabla.getSelectedRow(), 0)));
			if(!listaPpal.containsKey(Long.parseLong(txtNro.getText())))throw new ExcepcionDatos("No se encontro");
			if(listaPpal.remove(Long.parseLong(txtNro.getText()))==null)JOptionPane.showMessageDialog(null, "Borrado");
			mostrar(listaPpal);
		} catch (ExcepcionDatos e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		
	}

	protected void buscar() {
		try {
			if (!listaPpal.containsKey(Long.parseLong(txtNro.getText())))
				throw new ExcepcionDatos("No se encontro socio");
			Socio s = listaPpal.get(Long.parseLong(txtNro.getText()));
			Map<Long, Socio> unalista = new TreeMap<Long, Socio>();
			unalista.put(s.getNrosocio(), s);
			mostrar(unalista);

		} catch (ExcepcionDatos e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	protected void agregar() {

		try {
			long nro = Long.parseLong(txtNro.getText());
			String nombre = txtNombre.getText();
			String apellido = txtApellido.getText();
			long dni = Long.parseLong(txtDni.getText());
			Socio socio = new Socio(nombre, apellido, dni, nro, LocalDate.now());
			if (listaPpal.containsKey(socio.getNrosocio()))
				throw new ExcepcionDatos("Socio existente");
			listaPpal.put(socio.getNrosocio(), socio);
			mostrar(listaPpal);
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, e.getMessage());
		}

	}

	protected void guardar(Map<Long, Socio> listaPpal2) {

		List<Socio> arraySocios = new ArrayList<Socio>(listaPpal2.values());
		Persistencia.escribir(arraySocios);

	}

	protected void mostrar(Map<Long, Socio> lista) {
		Object[] key = lista.keySet().toArray();
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		modelo.setNumRows(0);
		modelo.setColumnCount(0);
		modelo.addColumn("Nro Socio");
		modelo.addColumn("Nombre");
		modelo.addColumn("Apellido");
		modelo.addColumn("Dni");
		modelo.addColumn("Fecha Insc");
		modelo.addColumn("Ult. Pago");
		//tabla.setColumnModel();

		for (int i = 0; i < key.length; i++) {
			Socio aux = lista.get(key[i]);
			Object[] renglon = aux.toString().split(";");
			// System.out.println(aux.toString());
			modelo.addRow(renglon);
		}

	}

	protected void cargarArchivo() {
		listaPpal.clear();
		List<String> datos = Persistencia.leer2();
		try {
			if (datos == null)
				throw new ExcepcionDatos("Archivo inexistente");
			for (String s : datos) {
				String[] sub = s.split(";");
				Socio socio = new Socio(Long.parseLong(sub[0]), sub[1], sub[2], Long.parseLong(sub[3]),
						LocalDate.parse(sub[4]), LocalDate.parse(sub[5]));

				listaPpal.put(socio.getNrosocio(), socio);
			}
		} catch (ExcepcionDatos E) {
			JOptionPane.showMessageDialog(null, E.getMessage());
		} catch (Exception E) {
			JOptionPane.showMessageDialog(null, E.getMessage());
		}
	}
}
