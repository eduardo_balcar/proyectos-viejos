package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class Persistencia {

	public static <T> boolean escribir(List<T> datos) {
		boolean ok = true;
		try {
			File archivo = new File(System.getProperty("user.dir") + "/src/archivos/datos.txt");// Si el archivo no existe es
																						// creado
			if (!archivo.exists()) {
				archivo.createNewFile();
			}
			FileWriter salida = new FileWriter(System.getProperty("user.dir") + "/src/archivos/datos.txt");
			BufferedWriter miBuffer = new BufferedWriter(salida);
			for (T dato : datos) {
				miBuffer.write(dato.toString());
				miBuffer.flush();
				miBuffer.newLine();
			}
			miBuffer.close();
		} catch (Exception e) {// TODO: handle exception}
			ok = false;
		}
		return ok;
	}

	public static <S> List<S> leer2() {

		List<S> lista = new ArrayList<S>();
		String linea = "";

		try {
			File archivo = new File(System.getProperty("user.dir") + "/src/archivos/datos.txt");
			if (!archivo.exists())
				throw new Exception("Archivo inexistente");

			FileReader entrada = new FileReader(System.getProperty("user.dir") + "/src/archivos/datos.txt");

			BufferedReader miBuffer = new BufferedReader(entrada); // creo el buffer para leer

			while (linea != null) {
				linea = miBuffer.readLine();// ledato una linea
				if (linea != null) {
					lista.add((S) linea);
				}
			}
			entrada.close(); // close the stream
			miBuffer.close(); // close the buffer
		} catch (Exception e) {
			lista = null;
		}

		return lista;
	}
}
