package clases;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Socio extends Persona implements Comparable {
	private long nrosocio;
	private LocalDate fechaInsc;
	private LocalDate fechaPago;
	DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public Socio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Socio(String nombre, String apellido, long dni, long nrosocio, LocalDate fechaPago) {
		super(nombre, apellido, dni);
		this.nrosocio = nrosocio;
		this.fechaInsc = LocalDate.now();
		this.fechaPago = fechaPago;
	}
	
	public Socio(long nrosocio,String nombre, String apellido, long dni,LocalDate fechaInsc, LocalDate fechaPago) {
		super(nombre, apellido, dni);
		this.nrosocio = nrosocio;
		this.fechaInsc = LocalDate.now();
		this.fechaPago = fechaPago;
	}

	public long getNrosocio() {
		return nrosocio;
	}

	public void setNrosocio(long nrosocio) {
		this.nrosocio = nrosocio;
	}

	public LocalDate getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(LocalDate fechaPago) {
		this.fechaPago = fechaPago;
	}

	@Override
	public String toString() {

		return String.valueOf(this.nrosocio) + ";" + this.getNombre() + ";" + this.getApellido() + ";"
				+ String.valueOf(this.getDni())+";" + String.valueOf(this.fechaInsc) + ";" + String.valueOf(this.fechaPago);
	}

	@Override
	public int compareTo(Object arg0) {
		Socio s = (Socio) arg0;
		return (this.getNombre().toLowerCase() + this.getApellido().toLowerCase())
				.compareTo(s.getNombre().toLowerCase() + s.getApellido().toLowerCase());
	}

}
