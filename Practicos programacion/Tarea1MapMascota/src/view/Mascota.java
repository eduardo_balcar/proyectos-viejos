package view;

public class Mascota implements Comparable {

	private int nro;
	private String nombre;
	private String raza;
	private int edad;
	
	public Mascota(int nro, String nombre, String raza, int edad) {
		super();
		this.nro = nro;
		this.nombre = nombre;
		this.raza = raza;
		this.edad = edad;
	}

	public int getNro() {
		return nro;
	}

	public void setNro(int nro) {
		this.nro = nro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public boolean equals(Object obj) {//se utiliza para comparar los value del map
		Mascota arg1=(Mascota) obj;
		
		if(arg1.getNombre().equals(this.nombre))return true;
		else return false;
	}

	@Override
	public int compareTo(Object arg0) {//se utiliza para comparar las key del map
		Mascota arg1=(Mascota) arg0;
		return arg1.getNro()-this.nro;
	}
	
	
	
	
}
