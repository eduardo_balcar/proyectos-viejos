package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JDesktopPane;
import java.awt.GridLayout;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ListSelectionModel;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class VentanaPrincipal extends JFrame {
	protected JPanel panel;
	protected JScrollPane scrollPane;
	protected JTable table;
	protected JLabel lblNewLabel;
	protected JLabel lblNewLabel_1;
	protected JLabel lblNewLabel_2;
	protected JLabel lblNewLabel_3;
	protected JTextField txtNro;
	protected JTextField txtNombre;
	protected JTextField txtRaza;
	protected JTextField txtEdad;
	protected JButton btnNuevo;
	protected JButton btnMod;
	protected JButton btnBorra;
	protected JButton btnSalir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		Map<Integer, Mascota> mapMascota = new TreeMap<Integer, Mascota>();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

		this.panel = new JPanel();
		this.panel.setBounds(0, 0, 442, 264);
		getContentPane().add(this.panel);
		this.panel.setLayout(null);

		this.scrollPane = new JScrollPane();
		this.scrollPane.setBounds(12, 155, 418, 97);
		this.panel.add(this.scrollPane);

		this.table = new JTable();
		this.table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// if(table.hasFocus())JOptionPane.showMessageDialog(null,
				// table.getSelectedRow());
				txtNro.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), 0)));
				txtNombre.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), 1)));
				txtRaza.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), 2)));
				txtEdad.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
			}
		});
		this.table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.table.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, },
				new String[] { "Nro", "Nombre", "Raza", "Edad" }));
		this.scrollPane.setViewportView(this.table);

		this.lblNewLabel = new JLabel("Nro");
		this.lblNewLabel.setBounds(12, 12, 70, 15);
		this.panel.add(this.lblNewLabel);

		this.lblNewLabel_1 = new JLabel("Nombre");
		this.lblNewLabel_1.setBounds(12, 45, 70, 15);
		this.panel.add(this.lblNewLabel_1);

		this.lblNewLabel_2 = new JLabel("Raza");
		this.lblNewLabel_2.setBounds(12, 77, 70, 15);
		this.panel.add(this.lblNewLabel_2);

		this.lblNewLabel_3 = new JLabel("Edad");
		this.lblNewLabel_3.setBounds(12, 109, 70, 15);
		this.panel.add(this.lblNewLabel_3);

		this.txtNro = new JTextField();
		this.txtNro.setBounds(108, 10, 114, 19);
		this.panel.add(this.txtNro);
		this.txtNro.setColumns(10);

		this.txtNombre = new JTextField();
		this.txtNombre.setBounds(108, 45, 114, 19);
		this.panel.add(this.txtNombre);
		this.txtNombre.setColumns(10);

		this.txtRaza = new JTextField();
		this.txtRaza.setBounds(108, 77, 114, 19);
		this.panel.add(this.txtRaza);
		this.txtRaza.setColumns(10);

		this.txtEdad = new JTextField();
		this.txtEdad.setBounds(108, 109, 114, 19);
		this.panel.add(this.txtEdad);
		this.txtEdad.setColumns(10);

		this.btnNuevo = new JButton("Agregar");
		this.btnNuevo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int nor = Integer.parseInt(txtNro.getText());
					int edad = Integer.parseInt(txtEdad.getText());
					String nombre = txtNombre.getText();
					String raza = txtRaza.getText();
					Mascota mascota = new Mascota(nor, nombre, raza, edad);
					if (mapMascota.containsKey(nor))
						throw new Exception("Nro ya ingresado");
					if( mapMascota.containsValue(mascota))
						throw new Exception("Nombre ya ingresado");
					mapMascota.put(mascota.getNro(), mascota);
					ver(mapMascota);
					limpiar();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Error en los datos " + e1.getMessage());
				}
			}

		});
		this.btnNuevo.setBounds(313, 7, 117, 25);
		this.panel.add(this.btnNuevo);

		this.btnMod = new JButton("Modificar");
		this.btnMod.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int nor = Integer.parseInt(txtNro.getText());
					int edad = Integer.parseInt(txtEdad.getText());
					String nombre = txtNombre.getText();
					String raza = txtRaza.getText();
					Mascota mascota = new Mascota(nor, nombre, raza, edad);
					if (mapMascota.containsKey(nor)) {
						mapMascota.replace(mascota.getNro(), mascota);
						ver(mapMascota);
						limpiar();
					} else
						throw new Exception("Nro no encontrado");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Error en los datos " + e1.getMessage());
				}
			}
		});
		this.btnMod.setBounds(313, 40, 117, 25);
		this.panel.add(this.btnMod);

		this.btnBorra = new JButton("Borrar");
		this.btnBorra.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int nor = Integer.parseInt(txtNro.getText());
					if (mapMascota.containsKey(nor)) {
						mapMascota.remove(nor);
						ver(mapMascota);
						limpiar();
						
					} else
						throw new Exception("Nro no encontrado");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Error en los datos " + e1.getMessage());
				}
			}
		});
		this.btnBorra.setBounds(313, 72, 117, 25);
		this.panel.add(this.btnBorra);

		this.btnSalir = new JButton("Salir");
		this.btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		this.btnSalir.setBounds(313, 104, 117, 25);
		this.panel.add(this.btnSalir);
		
	  

	}

	protected void limpiar() {
		txtNro.setText("");
		txtNombre.setText("");
		txtRaza.setText("");
		txtEdad.setText("");
	}

	protected void ver(Map<Integer, Mascota> mapMascota) {
		DefaultTableModel modelo = (DefaultTableModel) table.getModel();
		modelo.setNumRows(0);
		Object[] keys = mapMascota.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			Mascota aux = mapMascota.get(keys[i]);
			Object[] renglon = { aux.getNro(), aux.getNombre(), aux.getRaza(), aux.getEdad() };
			modelo.addRow(renglon);
		}
		
	}
}
