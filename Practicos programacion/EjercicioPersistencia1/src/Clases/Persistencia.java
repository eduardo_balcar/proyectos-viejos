package Clases;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

public class Persistencia {

	public static boolean escribir(Map<Integer, Socio> lista) {
		boolean ok = true;
		String dato;
		try {
			File archivo = new File(System.getProperty("user.dir") + "/datos.txt");// Si el archivo no existe es creado
			if (!archivo.exists()) {
				archivo.createNewFile();
			}
			Object[] key = lista.keySet().toArray();
			FileWriter salida = new FileWriter(System.getProperty("user.dir") + "/datos.txt");
			BufferedWriter miBuffer = new BufferedWriter(salida);
			for (int i = 0; i < key.length; i++) {
				Socio aux = (Socio) lista.get(key[i]);
				dato = String.valueOf(aux.getNro()) + ";" + aux.getNombre() + ";" + aux.getDni();
				miBuffer.write(dato);
				miBuffer.newLine();
				miBuffer.flush();
			}
			miBuffer.close();
		} catch (Exception e) {// TODO: handle exception}
			JOptionPane.showMessageDialog(null, e.getMessage());
			ok = false;
		}
		return ok;
	}

	public static HashMap<Integer, Socio> leer() {

		HashMap<Integer, Socio> lista = new HashMap<Integer, Socio>();

		try {
			File archivo = new File(System.getProperty("user.dir") + "/datos.txt");
			if (!archivo.exists())
				throw new Exception("Archivo inexistente");

			FileReader entrada = new FileReader(System.getProperty("user.dir") + "/datos.txt");

			BufferedReader miBuffer = new BufferedReader(entrada); // creo el buffer para leer
			String[] datos;
			String linea = "";
			while (linea != null) {
				linea = miBuffer.readLine();// ledato una linea
				if (linea != null) {
					datos = linea.split(";");
					lista.put(Integer.parseInt(datos[0]), new Socio(Integer.parseInt(datos[0]), datos[1], datos[2]));
				}
			}
			entrada.close(); // close the stream
			miBuffer.close(); // close the buffer
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());

		}

		return lista;
	}
}
