package Clases;

public class Socio implements Comparable<Socio> {
	private int nro;
	private String nombre;
	private String dni;
	
	public Socio(int nro, String nombre, String dni) {
		super();
		this.nro = nro;
		this.nombre = nombre;
		this.dni = dni;
	}

	public Socio() {
		super();
	}

	public int getNro() {
		return nro;
	}

	public void setNro(int nro) {
		this.nro = nro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public int compareTo(Socio arg0) {
		
		return arg0.getNro()-this.nro;
	}
	
	
	
	

}
