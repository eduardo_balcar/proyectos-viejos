package view;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Clases.Socio;
import Clases.Persistencia;

public class VentanaPpal extends JFrame {
	private JTextField txtNro;
	private JTextField txtNombre;
	private JTextField txtDni;
	private JTable tabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPpal frame = new VentanaPpal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static Map<Integer, Socio> lista = new HashMap<Integer, Socio>();

	/**
	 * Create the frame.
	 */
	public VentanaPpal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 142, 418, 110);
		getContentPane().add(scrollPane);

		tabla = new JTable();
		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row =tabla.getSelectedRow();
				txtNro.setText(String.valueOf(tabla.getValueAt(row, 0)));
				txtNombre.setText(String.valueOf(tabla.getValueAt(row, 1)));
				txtDni.setText(String.valueOf(tabla.getValueAt(row, 2)));
			}
		});
		tabla.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, },
				new String[] { "Nro", "Nombre", "DNI" }));
		scrollPane.setViewportView(tabla);

		JLabel lblNewLabel = new JLabel("Nro");
		lblNewLabel.setBounds(12, 12, 70, 15);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(12, 49, 70, 15);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("DNI");
		lblNewLabel_2.setBounds(12, 88, 70, 15);
		getContentPane().add(lblNewLabel_2);

		txtNro = new JTextField();
		txtNro.setBounds(100, 11, 64, 18);
		getContentPane().add(txtNro);
		txtNro.setColumns(10);

		txtNombre = new JTextField();
		txtNombre.setBounds(100, 48, 150, 18);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);

		txtDni = new JTextField();
		txtDni.setBounds(100, 87, 64, 18);
		getContentPane().add(txtDni);
		txtDni.setColumns(10);

		JButton btnleer = new JButton("Leer");
		btnleer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lista = Persistencia.leer();
				mostrar();
			}
		});
		btnleer.setBounds(177, 7, 75, 25);
		getContentPane().add(btnleer);

		JButton btnout = new JButton("Out");
		btnout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {

					Persistencia.escribir(lista);
					mostrar();
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(btnout, ee.getMessage());
				}
			}
		});
		btnout.setBounds(264, 7, 75, 25);
		getContentPane().add(btnout);

		JButton btnagregar = new JButton("Agregar");
		btnagregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if (lista.containsKey(Integer.parseInt(txtNro.getText())))
						throw new Exception("Ya agregado");
					Socio elemento = new Socio(Integer.parseInt(txtNro.getText()), txtNombre.getText(),
							txtDni.getText());
					lista.put(Integer.parseInt(txtNro.getText()), elemento);
					mostrar();
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(btnout, ee.getMessage());
				}
			}
		});
		btnagregar.setBounds(355, 7, 75, 25);
		getContentPane().add(btnagregar);

		JButton btnmodificar = new JButton("Modificar");
		btnmodificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
				if(lista.containsKey(Integer.parseInt(txtNro.getText()))) {
					Socio elemento= new Socio(Integer.parseInt(txtNro.getText()),txtNombre.getText(),txtDni.getText());
					lista.replace(Integer.parseInt(txtNro.getText()), elemento);
					mostrar();
				}
				else throw new Exception("Nro no encontrado..."); 
			}
				catch(Exception ee) {
					JOptionPane.showMessageDialog(null, ee.getMessage());
					}
				}
		});
		btnmodificar.setBounds(355, 44, 75, 25);
		getContentPane().add(btnmodificar);

		JButton btnsalir = new JButton("Salir");
		btnsalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		btnsalir.setBounds(355, 83, 75, 25);
		getContentPane().add(btnsalir);
		// getContentPane().setLayout(new BorderLayout(0, 0));
	}

	protected void mostrar() {
		Object[] key = lista.keySet().toArray();
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		modelo.setNumRows(0);
		for (int i = 0; i < key.length; i++) {
			Socio aux = lista.get(key[i]);
			Object[] renglon = { aux.getNro(), aux.getNombre(), aux.getDni() };
			modelo.addRow(renglon);
		}

	}
}
