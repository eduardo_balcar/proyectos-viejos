package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {

			AnchorPane raiz = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/VentanaPpal.fxml"));
			System.out.println("uno");
			Scene scene = new Scene(raiz, 600, 400);
			primaryStage.setTitle("Convertidor de millas");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("mal");
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
