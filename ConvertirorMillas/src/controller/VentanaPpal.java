package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;

public class VentanaPpal {

	public VentanaPpal() {
		// TODO Auto-generated constructor stub
	}
	
	@FXML
    private JFXButton btnconvertir;

    @FXML
    private JFXButton btnlimpiar;

    @FXML
    private JFXButton btnsalir;

    @FXML
    private Label labuno;

    @FXML
    private Label labdos;

    @FXML
    private Label labtres;
    
    @FXML
    private JFXTextField txtmillas;
    
    @FXML
    void initialize() {
        txtmillas.requestFocus();
        btnconvertir.setOnAction(e->{
        	calcularmillas();
        });
        
        btnlimpiar.setOnAction(e->{
        	txtmillas.clear();
        	txtmillas.requestFocus();
        	labdos.setText("---");
        });
        
        btnsalir.setOnAction(e->{
        	Platform.exit();
        	//System.exit(0);
        });

    }

	private void calcularmillas() {
		try {
			float millas=Float.parseFloat(txtmillas.getText());
			double km = 1000*millas/621.4;
			labdos.setText(String.valueOf(km));
		} catch (Exception e) {
			Alert exceptionDialog = new Alert(AlertType.WARNING);
			exceptionDialog.setTitle("Dato mal ingresado");
			exceptionDialog.setHeaderText("No ha ingresado un numero valido");
			exceptionDialog.setContentText("Compruebe el formato del numero ingresado");
			exceptionDialog.show();
		}
		
		
	}


}
