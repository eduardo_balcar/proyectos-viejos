Attribute VB_Name = "M�dulo2"
Sub hace_copia_part_mante_gal�n()

Application.ScreenUpdating = False
On Error Resume Next

Dim bandera, ref As String
Dim archivsplit() As String
Dim sala As String

bandera = ThisWorkbook.Name
ref = "Planilla"

If InStr(1, bandera, ref) = 1 Then
' copia_parte_mant_galan Macro
' hace copia de  parte de mantenimiento para guardar de sala galan
'
Dim nombre As String, nroinforme As String

Dim Ruta As String, archivo As String, ultArchivo As String
Dim ultFecha As Date, UFM As String 'fecha �ltima modificaci�n
Dim aux As String
Dim aux2 As String

aux2 = "0"
'Ruta = "C:\Users\Neogame\Desktop\Informes en excel\" 'para jockey
'Ruta = "C:\Area Tecnica\INFORMES TECNICOS\INFORMES EXCEL\" ' para galan
Ruta = Sheets("Parametros").Range("B2").Value + "\"
sala = Sheets("Parametros").Range("B3").Value

archivo = Dir(Ruta & "*.xls*", vbNormal)

Sheets("PARTE MANTENIMIENTO").Select

If Range("D14").Text <> "" Then
If Len(archivo) = 0 Then
    'verificamos que existe alg� nfichero de Excel en la carpeta
    MsgBox "Mala suerte... No existe ning�n archivo de Excel en esta carpeta", vbExclamation
    Exit Sub    'salimos del procedimiento
End If

 

'recorremos todos los ficheros de Excel existentes
Do While Len(archivo) > 0
    
    UFM = archivo
    archivosplit = Split(UFM)
    'aux = Mid(UFM, 17, 4)
    aux = Val(Mid(archivosplit(2), 2))
    If StrComp(aux, aux2) = 1 Then
        aux2 = aux
        ultArchivo = archivo
    End If
    'liberamos la variable y preparamos para el siguiente archivo
    archivo = Dir
Loop

nroinforme = InputBox("INGRESE EL NRO DE INFORME" & vbNewLine & vbNewLine & "Ultimio informe creado: " & vbNewLine & ultArchivo, "Menu COPIA", aux2 + 1)

If nroinforme = vbNullString Then
MsgBox ("Cancelado"), vbInformation
Exit Sub
End If

''DESPROTECCION
ActiveSheet.Unprotect 123
Application.DisplayAlerts = False
Range("H2").Value = nroinforme
MsgBox ("Se va a generar el Informe N: " & nroinforme), vbOKCancel
nombre = Sheets("Parametros").Range("B4").Value + nroinforme + " - " + Range("D14").Text + " " + Range("D11").Text

'Se borran las hojas que no se ocupan
Sheets("PARTE CAMBIO").Delete

Sheets("Parametros").Delete

Sheets("PARTE MANTENIMIENTO").Select
   With ActiveSheet.UsedRange
    .Value = .Value
    End With
    Range("A3").Value = sala
    Application.CutCopyMode = False
    ActiveSheet.Cells.Interior.ColorIndex = -1
    
    'MsgBox (Range("D13"))
    If InStr(1, Range("D13").Value, "trimline", 1) >= 1 Then
    Range("F36:H52").NumberFormat = "#,000"
    Else
    Range("F36:H52").NumberFormat = "$#,##0.00"
    End If
    
    For i = 36 To 52
    If InStr(1, Range("A" & i).Value, "games", 1) > 0 Then
    Range("F" & i & ":H" & i).NumberFormat = "General"
    End If
    Next
    
    Application.ScreenUpdating = True
    
    aux = ThisWorkbook.Path + "\" + ThisWorkbook.Name
    ActiveWorkbook.SaveAs Filename:=Ruta & nombre, FileFormat:=xlOpenXMLWorkbookMacroEnabled
    Workbooks.Open Filename:=aux
    ''PROTECCION
    ActiveSheet.Protect 123
    ActiveWorkbook.Save
    Else
    MsgBox ("No se agrego nro de maquina..."), vbCritical
    End If

Else
MsgBox ("Funci�n DESHABILITADA: Estas en una copia ya guardada..."), vbCritical
End If
    
Application.DisplayAlerts = True

End Sub
