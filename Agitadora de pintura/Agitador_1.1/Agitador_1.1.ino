//Programa para agitadora de pintura

//Nombre para los pines de salida
  int mprensaA = 6; //Digital - motor prensa Arriba
  int mprensaB = 12; //Digital - motor prensa Bajar
  int magitador = 10; //Digital
  int luzverde= 11; //Digital

  //Nombre para los pines de entrada
  int puerta = 9; //Digital
  int botonrojo = 8;//Digital
  int botonverde = 7;//Digital
  int sencorriente = A0; //Analogico
  int senpote = A1; //Analogico

//Variables de entrtadas
  int valcorriente = 0;
  int valpote = 0;
  long tiempo;

  bool puertaCerrada = false;
  bool prensaAbierta = false;
  bool agitando = false;
  bool inicio = true;
  bool fallo =false;
  bool verde = false;

  int bufer;

void setup() {
    
  //Definicion de los pines
  pinMode(mprensaA, OUTPUT);//pin para subir la prensa
  
  pinMode(mprensaB, OUTPUT);//pin para bajar la prensa
  
  pinMode(magitador, OUTPUT);//pin para accionar el motor del agitador
  
  pinMode(luzverde, OUTPUT);//pin que cambia el estado de la luz roja a verde
  

  pinMode(puerta, INPUT_PULLUP);//pin que verifica si la puerta esta abierta
  pinMode(botonrojo, INPUT_PULLUP);//pin del boton rojo
  pinMode(botonverde, INPUT_PULLUP);//pin del boton verde
  pinMode(sencorriente, INPUT);//pin que lee el valor de la corriente del sensor del motoro de 24v
  pinMode(senpote, INPUT);//pin que lee el valor del potenciometro del tiempo

  digitalWrite(mprensaA,1);
  digitalWrite(mprensaB,1);
  digitalWrite(magitador, 1);
  digitalWrite(luzverde, 1);
  Serial.begin(9600);
  inicio=true;
  bufer=0;

}
//---------------------------------------------------------------------------
void loop() {
  
 if(inicio){
  verificar();
 }

if(digitalRead(botonverde)==0 && digitalRead(puerta)==0){
    delay(400);
    if(digitalRead(botonverde)==0 && digitalRead(puerta)==0 ){
      verde = false;
      digitalWrite(luzverde, 1);
      leertiempo();
      cerrarprensa();
      if(!fallo) agitar();
      abrirprensa();
      verde = true;
      if(!fallo) digitalWrite(luzverde, 0);
      }
   }
}
//---------------------------------------------------------------------------
void verificar (){

  //Se abre la prensa
  //Inicia con la luz roja encendida
  digitalWrite(mprensaA,0);
  Serial.println("subiendo");
  valcorriente = analogRead(A0);
  verde=false;

  while(valcorriente < 525){
    valcorriente = analogRead(A0);
    Serial.println(valcorriente);
  }
  
  Serial.println("corte");
  digitalWrite(mprensaA,1);
  delay(500);

  //Se cierra la prens durante 5 segundos: corta por consumo (error) o por tiempo (ok)
  digitalWrite(mprensaB,0);
  Serial.println("bajando");
  tiempo = millis();
  
  while(millis()-tiempo < 5000){
    valcorriente = analogRead(A0);
    Serial.println(valcorriente);
    if(valcorriente < 493){
      digitalWrite(mprensaB,1);
      error();
    }
  }

  digitalWrite(mprensaB,1);

  //Se repite el codigo de abrir la prensa si no hubo fallo
  if(!fallo){
  digitalWrite(mprensaA,0);
  valcorriente = analogRead(A0);
  while(valcorriente < 525){
    valcorriente = analogRead(A0);
    delay(100);
  }

  digitalWrite(mprensaA,1);
  digitalWrite(luzverde,0);
  verde=true;
  prensaAbierta = true;
  }
  
  Serial.println("final");
  inicio = false;
}
//---------------------------------------------------------------------------

//Si se produce error se para todo
void error(){
  digitalWrite(mprensaA, 1);
  digitalWrite(mprensaB, 1);
  digitalWrite(magitador, 1);
  digitalWrite(luzverde, 1);
  fallo = true;
}

//---------------------------------------------------------------------------
void leertiempo(){
  valpote = analogRead(senpote);
  tiempo = valpote * 180000 / 1024;
  }

//---------------------------------------------------------------------------
void cerrarprensa(){
  digitalWrite(mprensaB, 0);
  bool continuar=true;
  delay(2000);
  
  valcorriente = analogRead(sencorriente);
  Serial.println("Cerrando prensa");
  
  while(valcorriente > 486 && continuar){
    
    if(digitalRead(botonrojo)==LOW)continuar=false;
    if(digitalRead(puerta)==1){
      digitalWrite(mprensaB,1);
      delay(300);
      continuar=false;
      while(!continuar){
        if(digitalRead(puerta)==0)continuar=true;
        }
        digitalWrite(mprensaB,0);
    }
    valcorriente = analogRead(sencorriente);
    Serial.println(valcorriente);
    delay(100);
    }

    digitalWrite(mprensaB, 1);
    if(!continuar){
      abrirprensa();
      fallo=true;
      }
  
  Serial.println("Cerra prensa listo");
  }
//---------------------------------------------------------------------------
void agitar(){
  long comienzo = millis();
  bool continuar=true;
  Serial.println("Agitando");
  Serial.println(millis()-comienzo);

  while(millis()-comienzo < 5000){
    Serial.println("Esperando acomodar tacho");
    if(digitalRead(puerta)==1){
      continuar=false;
      while(!continuar){
        if(digitalRead(puerta)==0)continuar=true;
        comienzo=millis();
        }
    }
    }

  comienzo = millis();
  digitalWrite(magitador, 0);
  
  while(millis()-comienzo < tiempo){
    Serial.println("Agitando tiempo: ");
    Serial.print(millis()-comienzo );
    Serial.println(tiempo);

    if(digitalRead(puerta)==1){
      continuar=false;
      digitalWrite(magitador,1);
      delay(300);
      while(!continuar){
        if(digitalRead(puerta)==0)continuar=true;
        }
        digitalWrite(magitador,0);
    }

    if(!digitalRead(botonrojo)){
      digitalWrite(magitador, 1);
      tiempo = 0;
      continuar=false;
      fallo=true;
      }  
    }
    digitalWrite(magitador,1);

    comienzo = millis();
    while(millis()-comienzo < 8000){
    Serial.println("Esperando relajacion");
    }
}
    if(!continuar)abrirprensa();
  
  Serial.println("Fin agitador");
  }
//---------------------------------------------------------------------------
void abrirprensa(){
  digitalWrite(mprensaA, 0);
  delay(2000);

  Serial.println("Abriendo prensa");
  while(valcorriente < 525){
    valcorriente = analogRead(sencorriente);
    }

  digitalWrite(mprensaA, 1);
  Serial.println("Fin abrir despues de agitar");
  }
